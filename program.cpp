#include <iostream>
#include <string.h>

using namespace std;


// produse, clienti, furnizori, 


// produs - id, nume, descriere, pret
class Produs{
public:


    // metode speciale
    // Produs p99;
    Produs(double p){
        cout <<"PRODUS NOU........................................................"<<endl;
        pret = p;
    }

    // setter pentru VALIDARE
    void setIdentificator(int id){
        if(id > 5000 && id < 20000){
            identificator = id;
        }else{
            identificator = -1;
        }
    }
    
    // setter pentru VALIDARE
    void setPret(double p){ 
        if(p < 100){
            pret = 100;
        }else{
            pret = p;
        }
    }
    
    void afisareProdus(){ // nu poate fi static, deoarece foloseste field-urile
        cout <<"Nume = " << numeProdus << " descriere: " << descriereProdus << " ID " << identificator << " costa: " << pret << endl;
    }
    
    // setter pentru ALOCARE
    void setNumeProdus(char * numeProdus){
        this->numeProdus = new char[strlen(numeProdus)+1];
        strcpy(this->numeProdus, numeProdus);
    }
    
    void setDescriere(char * descriere){
        descriereProdus = new char[strlen(descriere)+1];
        strcpy(descriereProdus, descriere);
    }
    
    char * getNumeProdus(){
        return this->numeProdus;
    }
    
private:
    char * numeProdus;
    char * descriereProdus;
    double pret;
    int identificator;
};


class Mate{

public:

    static int suma(int x, int y){
        return x+y;
    }
private:
};


int main(){
    
    
    cout <<"===================================="<<endl;
    
    // Mate m;
    // cout << m.suma(1,2) << endl;
    // Mate m2;
    // cout << m2.suma(1,2) << endl;
    cout << Mate::suma(1,2) << endl;
    
    
    cout <<"===================================="<<endl;
    
    // Produs p1;
    // p1.pret = 99.99;
    // p1.identificator = 1001;
    // p1.numeProdus = "Laptop";
    // p1.numeProdus = new char[64];
    // strcpy(p1.numeProdus, "Laptop");
    // p1.descriereProdus = new char[200];
    // strcpy(p1.descriereProdus, "Laptop ASUS foarte okay");
    
    // Produs p2;
    // p2.pret = 200;
    // p2.identificator = 1002;
    
    
    // cout <<"Pret: " << p1.pret << " ID = " << p1.identificator << " " << p1.numeProdus <<endl;
    // p1.afisareProdus();
    
    Produs p3(99.99);  // PRODUS NOU........................................................
    p3.setNumeProdus("Desktop"); // aloca memorie si pune valoarea 'unde trebuie'
    p3.setDescriere("Desktop myria i5 6gb ram...");
    p3.setIdentificator(99);
    
    p3.afisareProdus();
    
    Produs p4(100.0);
    // strcpy(p4.numeProdus, "TEST"); - eroare, nu am alocat memorie
    p4.setNumeProdus("TEST");
    p4.setDescriere("Un test reusit");
    
    
    p4.setIdentificator(10);
    
    p4.setPret(-44.1);
    // p4.pret = -44.0;
    
    p4.afisareProdus();
    // cout << p4.numeProdus << endl; - numeProdus este private, nu il mai putem accesa direct
    cout << p4.getNumeProdus() << endl;
  
    cout <<"END"<<endl;
    
    
    
    cout <<"==================================================================="<< endl;
    
    Produs p5(300.99); // Produs(){... }
    p5.setNumeProdus("Monitor");
    p5.setDescriere("Monitor BENQ");
    // p5.setPret(3000);
    p5.afisareProdus();
    return 0;
}